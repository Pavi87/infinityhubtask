// ignore_for_file: library_private_types_in_public_api, prefer_typing_uninitialized_variables

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:infinityhub/widgets/getstoreage.dart';
import 'package:infinityhub/widgets/headerstyle.dart';
import 'package:infinityhub/widgets/qrscanview.dart';
import 'package:infinityhub/widgets/theme.dart';
import 'package:qr_flutter/qr_flutter.dart';

class QRCodeScreen extends StatefulWidget {
  const QRCodeScreen({super.key});

  @override
  _QRCodeScreenState createState() => _QRCodeScreenState();
}

class _QRCodeScreenState extends State<QRCodeScreen> {
  @override
  void initState() {
    super.initState();
    checkUserWallet();
  }

  bool loading = true;
  var localdata;

  var localdata1;
  checkUserWallet() async {
    localdata = await readData("UserData");
    Map<String, dynamic> data = json.decode(localdata);
    setState(() {
      localdata = data;
      loading = false;
    });
  }

  bool scanerrormsg = false;
  Future<dynamic> validateQRCodeData(String qrCodeData) async {
    List<String> data = qrCodeData.split(',');

    if (data.length != 4) {
      return null;
    }

    String firstName = data[0];
    String lastName = data[1];
    String email = data[2];
    String password = data[3];

    return (
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundfillcolor,
      appBar: AppBar(
        leading: const SizedBox(),
        toolbarHeight: 150,
        flexibleSpace: const HeaderWidget(
          heading: "Qr Code Generate",
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        child: loading == true
            ? const Center(
                child: SpinKitCircle(
                  color: primaryColor,
                  size: 50,
                ),
              )
            : Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    generateQRCode(
                      (
                        firstName: localdata['firstName'],
                        lastName: localdata['lastName'],
                        email: localdata['email'],
                        password: localdata['password'],
                      ),
                    ),
                    const SizedBox(height: 20.0),
                    ElevatedButton(
                      onPressed: () async {
                        var data = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => const QRScanView()));

                        if (data != null) {
                          var validate = await validateQRCodeData(data);
                          if (validate != null) {
                            setState(() {
                              localdata1 = validate;
                              scanerrormsg = false;
                            });
                          } else {
                            setState(() {
                              localdata1 = null;
                              scanerrormsg = true;
                            });
                          }
                        } else {
                          setState(() {
                            localdata1 = null;
                            scanerrormsg = true;
                          });
                        }
                      },
                      child: const Text('Scan QR Code'),
                    ),
                    const SizedBox(height: 20.0),
                    if (localdata1 != null)
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "First Name",
                                style: text1,
                              ),
                              Text(
                                localdata1!.firstName,
                                style: text1,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Last Name",
                                style: text1,
                              ),
                              Text(
                                localdata1!.lastName,
                                style: text1,
                                textAlign: TextAlign.left,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Email",
                                style: text1,
                              ),
                              Text(
                                localdata1!.email,
                                style: text1,
                              ),
                            ],
                          ),
                          const SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                "Password ",
                                style: text1,
                              ),
                              Text(
                                localdata1!.password,
                                style: text1,
                              ),
                            ],
                          ),
                        ],
                      ),
                    scanerrormsg == true
                        ? const Text("Invalid Data")
                        : const SizedBox()
                  ],
                ),
              ),
      ),
    );
  }

  Widget generateQRCode(userData) {
    String registrationData =
        '${userData.firstName},${userData.lastName},${userData.email},${userData.password}';

    return QrImageView(
      data: registrationData,
      dataModuleStyle: const QrDataModuleStyle(
        dataModuleShape: QrDataModuleShape.circle,
        color: Colors.black,
      ),
      eyeStyle: const QrEyeStyle(
        eyeShape: QrEyeShape.circle,
        color: Colors.black,
      ),
      size: 200.0,
    );
  }
}
