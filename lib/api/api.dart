// ignore_for_file: use_build_context_synchronously

import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:infinityhub/qrscan/qrscan.dart';
import 'package:infinityhub/signup/singup.dart';

import '../widgets/getstoreage.dart';

FirebaseFirestore firestore = FirebaseFirestore.instance;

class UserRepository {
  // User Registration //
  Future<dynamic> signUp(
    BuildContext context,
    String name,
    String lastName,
    String email,
    String password,
  ) async {
    debugPrint("0bodyres ");

    try {
      await firestore.collection('Users').doc(email).set({
        'firstName': name,
        'lastName': lastName,
        'email': email.toLowerCase().trim(),
        'password': password,
      }).then((value) async {
        var userInfo = {
          "firstName": name,
          "lastName": lastName,
          'email': email.toLowerCase().trim(),
          'password': password,
        };
        await writeData("UserData", jsonEncode(userInfo));

        debugPrint("user details added to fireStore ");
        Navigator.of(context).push(
          PageRouteBuilder(
            transitionDuration: const Duration(milliseconds: 800),
            reverseTransitionDuration: const Duration(milliseconds: 800),
            pageBuilder: (context, animation, secondaryAnimation) =>
                const QRCodeScreen(),
            transitionsBuilder:
                (context, animation, secondaryAnimation, child) {
              final tween = Tween(begin: 0.0, end: 1.0);
              final fadeAnimation = animation.drive(tween);
              return FadeTransition(
                opacity: fadeAnimation,
                child: child,
              );
            },
          ),
        );
      });
      return {'status': true, 'message': 'The account added.'};
      // return true;
    } catch (e) {
      return {
        'status': false,
        'message': 'The account already exists for that email.'
      };
    }
  }

// CheckAuth Status//
  void checkAuthState(BuildContext context) async {
    var user = await readData("UserData");
    debugPrint("user $user");
    if (user != null && user.isNotEmpty) {
      Navigator.of(context).push(
        PageRouteBuilder(
          transitionDuration: const Duration(milliseconds: 800),
          reverseTransitionDuration: const Duration(milliseconds: 800),
          pageBuilder: (context, animation, secondaryAnimation) =>
              const QRCodeScreen(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            final tween = Tween(begin: 0.0, end: 1.0);
            final fadeAnimation = animation.drive(tween);
            return FadeTransition(
              opacity: fadeAnimation,
              child: child,
            );
          },
        ),
      );
    } else {
      Navigator.of(context).push(
        PageRouteBuilder(
          transitionDuration: const Duration(milliseconds: 800),
          reverseTransitionDuration: const Duration(milliseconds: 800),
          pageBuilder: (context, animation, secondaryAnimation) =>
              const SignupPage(),
          transitionsBuilder: (context, animation, secondaryAnimation, child) {
            final tween = Tween(begin: 0.0, end: 1.0);
            final fadeAnimation = animation.drive(tween);
            return FadeTransition(
              opacity: fadeAnimation,
              child: child,
            );
          },
        ),
      );
    }
    // User? loged = _firebaseAuth.currentUser;
    // if (loged == null) {

    // } else if (loged.uid != null) {
    //   await getCurrentUser();
    //   Navigator.pushAndRemoveUntil(
    //     context,
    //     FadeRoute(
    //       page: const Tabscreen(),
    //     ),
    //     (Route<dynamic> route) => false,
    //   );
    // } else {
    //   Navigator.of(context).push(
    //     PageRouteBuilder(
    //       transitionDuration: const Duration(milliseconds: 800),
    //       reverseTransitionDuration: const Duration(milliseconds: 800),
    //       pageBuilder: (context, animation, secondaryAnimation) =>
    //           const LoginPage(),
    //       transitionsBuilder: (context, animation, secondaryAnimation, child) {
    //         final tween = Tween(begin: 0.0, end: 1.0);
    //         final fadeAnimation = animation.drive(tween);
    //         return FadeTransition(
    //           opacity: fadeAnimation,
    //           child: child,
    //         );
    //       },
    //     ),
    //   );
    // }
  }
}
