// ignore_for_file: library_private_types_in_public_api, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:infinityhub/widgets/theme.dart';

import '../api/api.dart';

// import 'package:stream_chat_flutter_core/stream_chat_flutter_core.dart';
class Splash extends StatefulWidget {
  final client;

  const Splash({Key? key, this.client}) : super(key: key);
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> with TickerProviderStateMixin {
  bool _visible = false;
  @override
  void initState() {
    Future.delayed(const Duration(milliseconds: 1000), () {
      setState(() {
        _visible = !_visible;
      });
      Future.delayed(const Duration(seconds: 2), () {
        UserRepository().checkAuthState(context);
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundfillcolor,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        color: backgroundfillcolor,
        alignment: Alignment.center,
        child: AnimatedOpacity(
          // If the widget is visible, animate to 0.0 (invisible).
          // If the widget is hidden, animate to 1.0 (fully visible).
          opacity: _visible ? 1.0 : 0.0,
          duration: const Duration(milliseconds: 400),
          // The green box must be a child of the AnimatedOpacity widget.

          child: Image.asset(
            'assets/loading.gif',
            fit: BoxFit.fitHeight,
            // color: primaryColor,
          ),
        ),
      ),
    );
  }
}
