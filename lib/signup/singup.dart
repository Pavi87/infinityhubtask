// ignore_for_file: library_private_types_in_public_api, non_constant_identifier_names, use_build_context_synchronously

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinityhub/qrscan/qrscan.dart';
import 'package:infinityhub/widgets/buttonfield.dart';
import 'package:infinityhub/widgets/headerstyle.dart';
import 'package:infinityhub/widgets/theme.dart';

import '../api/api.dart';
import '../widgets/textformfield.dart';

class SignupPage extends StatefulWidget {
  const SignupPage({Key? key}) : super(key: key);

  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController firstNamecontroller = TextEditingController();
  TextEditingController lastNamecontroller = TextEditingController();
  TextEditingController emailNamecontroller = TextEditingController();
  TextEditingController passwordNamecontroller = TextEditingController();
  RegExp pass_valid = RegExp(r"(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*\W)");
  //A function that validate user entered password
  bool validatePassword(String pass) {
    String password = pass.trim();
    if (pass_valid.hasMatch(password)) {
      if (password.length >= 8) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  bool _passwordVisible = false, loading = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundfillcolor,
      appBar: AppBar(
        leading: const SizedBox(),
        toolbarHeight: 150,
        flexibleSpace: const HeaderWidget(
          heading: "User Registration",
        ),
      ),
      body: SingleChildScrollView(
        padding: const EdgeInsets.all(10),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const SizedBox(
                height: 20,
              ),
              TextFormField(
                controller: firstNamecontroller,
                keyboardType: TextInputType.text,
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      color: customeblack,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(
                    RegExp("[a-zA-Z]"),
                  ),
                ],
                decoration: buildInputDecoration(
                    Icons.person, "First Name", false, _passwordVisible),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please a Enter your FirstName';
                  }
                  if (!RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                    return 'Please a valid Name';
                  }
                  return null;
                },
                onSaved: (value) {},
                autovalidateMode: AutovalidateMode.onUserInteraction,
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                controller: lastNamecontroller,
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      color: customeblack,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(
                    RegExp("[a-zA-Z]"),
                  ),
                ],
                keyboardType: TextInputType.text,
                decoration: buildInputDecoration(
                    Icons.person, "Last Name", false, _passwordVisible),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please a Enter your LastName';
                  }
                  if (!RegExp(r'^[a-z A-Z]+$').hasMatch(value)) {
                    return 'Please a valid Name';
                  }
                  return null;
                },
                onSaved: (value) {},
                autovalidateMode: AutovalidateMode.onUserInteraction,
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                controller: emailNamecontroller,
                keyboardType: TextInputType.text,
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      color: customeblack,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                decoration: buildInputDecoration(
                    Icons.email, "Email", false, _passwordVisible),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please a Enter your Email';
                  }
                  if (!RegExp("^[a-zA-Z0-9+_.-]+@[a-zA-Z0-9.-]+.[a-z]")
                      .hasMatch(value)) {
                    return 'Please a valid Email';
                  }
                  return null;
                },
                onSaved: (value) {
                  // email = value;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                controller: passwordNamecontroller,
                keyboardType: TextInputType.text,
                style: GoogleFonts.openSans(
                  textStyle: TextStyle(
                      color: customeblack,
                      fontSize: 14,
                      fontWeight: FontWeight.w500),
                ),
                obscureText: !_passwordVisible,
                decoration: InputDecoration(
                  hintText: "Password",
                  hintStyle: GoogleFonts.openSans(
                    textStyle: const TextStyle(
                        fontWeight: FontWeight.w400,
                        color: Color(0xff75788D),
                        fontSize: 14),
                  ),
                  suffixIcon: IconButton(
                    icon: Icon(
                      _passwordVisible
                          ? Icons.visibility
                          : Icons.visibility_off_outlined,
                      color: const Color(0xff75788D),
                      size: 20,
                    ),
                    onPressed: () {
                      setState(() {
                        _passwordVisible = !_passwordVisible;
                      });
                    },
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: const BorderSide(color: primaryColor, width: 1),
                  ),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: const BorderSide(
                      color: primaryColor,
                      width: 1,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: const BorderSide(
                      color: Color(0xffEFEFEF),
                      width: 1,
                    ),
                  ),
                  errorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8.0),
                    borderSide: const BorderSide(
                      color: Colors.red,
                      width: 1,
                    ),
                  ),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please a Enter your password';
                  } else {
                    //call function to check password
                    bool result = validatePassword(value);
                    if (result) {
                      return null;
                    } else {
                      return " Password should contain 8 Characters, Capital, small letter & Number & Special";
                    }
                  }
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
              ),
              const SizedBox(
                height: 40,
              ),
              MyButton(
                isDense: loading,
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    setState(() {
                      loading = true;
                    });
                    var response = await UserRepository().signUp(
                        context,
                        firstNamecontroller.text,
                        lastNamecontroller.text,
                        emailNamecontroller.text,
                        passwordNamecontroller.text);
                    debugPrint("successful ${response["status"]}");
                    if (response["status"] == true) {
                      debugPrint("successful ");

                      Navigator.of(context).push(
                        PageRouteBuilder(
                          transitionDuration: const Duration(milliseconds: 100),
                          reverseTransitionDuration:
                              const Duration(milliseconds: 100),
                          pageBuilder:
                              (context, animation, secondaryAnimation) =>
                                  const QRCodeScreen(),
                          transitionsBuilder:
                              (context, animation, secondaryAnimation, child) {
                            final tween = Tween(begin: 0.0, end: 1.0);
                            final fadeAnimation = animation.drive(tween);
                            return FadeTransition(
                              opacity: fadeAnimation,
                              child: child,
                            );
                          },
                        ),
                      );
                      setState(() {
                        loading = false;
                      });
                    } else {
                      setState(() {
                        loading = false;
                      });
                      // ToastMessage.toast(response["message"]);
                      debugPrint("UnSuccessfull");
                    }
                    return;
                  } else {
                    setState(() {
                      loading = false;
                    });
                    debugPrint("UnSuccessfull");
                  }
                },
                text: 'Create Account',
              ),
            ],
          ),
        ),
      ),
    );
  }
}
