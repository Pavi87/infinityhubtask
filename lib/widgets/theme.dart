// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:google_fonts/google_fonts.dart';

var customeblack = const Color(0xff000000);

var customewhite = const Color(0xffffffff);
const backgroundfillcolor = Color(0xffFAFAFA);
const primaryColor = Colors.purple;

var heading = GoogleFonts.openSans(
  textStyle: TextStyle(
    color: customewhite,
    fontSize: 20,
    fontWeight: FontWeight.w700,
  ),
);
var buttontext = GoogleFonts.openSans(
  textStyle: TextStyle(
    color: customewhite,
    fontSize: 18,
    height: 1.5,
    fontWeight: FontWeight.w600,
  ),
);
var text1 = GoogleFonts.openSans(
  textStyle: TextStyle(
    color: customeblack,
    fontSize: 16,
    height: 1.5,
    fontWeight: FontWeight.w500,
  ),
);
Center LOADING = const Center(
    child: SpinKitThreeBounce(
  color: primaryColor,
  size: 30,
));
Center LOADING1 = Center(
    child: SpinKitThreeBounce(
  color: customewhite,
  size: 30,
));
