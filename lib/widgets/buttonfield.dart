
import 'package:flutter/material.dart';
import 'package:infinityhub/widgets/theme.dart';

class MyButton extends StatelessWidget {
  final String text;
  final Function onPressed;
  final bool isDense;
@override
  const MyButton({super.key, 
    required this.text,
    required this.onPressed,
    required this.isDense,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45,
      width: MediaQuery.of(context).size.width - 20,
      child: MaterialButton(
        color: primaryColor,
        // onPressed: onPressed,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
            side: const BorderSide(color: primaryColor, width: 2)),
        textColor: Colors.white,
        onPressed: isDense ? null : onPressed as void Function(),
        child: isDense
            ? LOADING
            : Text(
                text,
                style: buttontext,
              ),
      ),
    );
  }
}
