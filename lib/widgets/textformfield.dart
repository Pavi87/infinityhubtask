import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:infinityhub/widgets/theme.dart';

InputDecoration buildInputDecoration(
  IconData icons,
  String hinttext,
  bool suffix,
  bool passwordVisible,
) {
  return InputDecoration(
      hintText: hinttext,
      hintStyle: GoogleFonts.openSans(
        textStyle: const TextStyle(
            fontWeight: FontWeight.w400,
            color: Color(0xff75788D),
            fontSize: 14),
      ),
      // prefixIcon: Icon(icons),
      // filled: true,
      // fillColor: Color(0xffF0F0F0),
      suffixIcon: suffix == false
          ? null
          : IconButton(
              icon: Icon(
                // Based on passwordVisible state choose the icon
                passwordVisible ? Icons.visibility : Icons.visibility_off,
                //  color: Theme.of(context).primaryColorDark,
              ),
              onPressed: () {
                // Update the state i.e. toogle the state of passwordVisible variable
                // setState(() {
                passwordVisible = !passwordVisible;
                // });
              },
            ),
      focusedBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(color: primaryColor, width: 1),
      ),
      border: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(
          color: primaryColor,
          width: 1,
        ),
      ),
      enabledBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(
          color: Color(0xffEFEFEF),
        ),
      ),
      errorBorder: OutlineInputBorder(
        borderRadius: BorderRadius.circular(8.0),
        borderSide: const BorderSide(
          color: Colors.red,
          width: 1,
        ),
      ),
      prefixIconColor: MaterialStateColor.resolveWith((states) =>
          states.contains(MaterialState.focused)
              ? primaryColor
              : const Color(0xffEEEEEE)));
}
