import 'package:flutter/material.dart';
import 'package:scan/scan.dart';

class QRScanView extends StatefulWidget {
  const QRScanView({Key? key}) : super(key: key);

  @override
  QRScanViewState createState() => QRScanViewState();
}

class QRScanViewState extends State<QRScanView> {
  final ScanController controller = ScanController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: ScanView(
          controller: controller,
          scanAreaScale: 1,
          scanLineColor: Theme.of(context).primaryColor,
          onCapture: (data) {
            Navigator.pop(context, data);
          },
        ),
      ),
    );
  }
}
