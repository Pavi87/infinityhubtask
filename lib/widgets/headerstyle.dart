// ignore_for_file: library_private_types_in_public_api, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:infinityhub/widgets/theme.dart';

class HeaderWidget extends StatefulWidget {
  final heading;
  const HeaderWidget({Key? key, this.heading}) : super(key: key);

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipPath(
          clipper: WaveClipperTwo(flip: true),
          child: Container(
            height: 200,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.purple,
                  Colors.deepPurpleAccent,
                  Colors.blueAccent
                ],
              ),
            ),
          ),
        ),
        ClipPath(
          clipper: WaveClipperTwo(),
          child: Container(
            height: 200,
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topLeft,
                end: Alignment.bottomRight,
                colors: [
                  Colors.purple,
                  Colors.deepPurpleAccent,
                  Colors.blueAccent
                ],
              ),
            ),
          ),
        ),
        Positioned(
            top: 100,
            left: 30,
            child: Text(
              widget.heading,
              style: heading,
            ))
      ],
    );
  }
}
